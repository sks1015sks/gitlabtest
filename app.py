# -*- coding: utf-8 -*-
import urllib.request
import re
import string

from urllib import parse
from bs4 import BeautifulSoup
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

SLACK_TOKEN = "xoxb-691472599991-678213940530-bbhG6POvnX5TmpcNtW7EIRIx"
SLACK_SIGNING_SECRET = "2d163dd8ae6748665d841f71ca941b6b"

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현하기
def _crawl_portal_keywords(text):
    # 입력 텍스트 공백으로 나누기
    text = text.strip().split()
    # 모든 입력 처음에 채널인지 아이디인지가 따라오므로 삭제
    del text[0]

    # 길이가 1 이면 단어, 2 이상이면 숙어
    # 멘션만 하고 검색 단어가 없다면 봇 소개와 사용법 출력
    if len(text) == 1:
        text = text[0]
    elif len(text) > 1:
        text = ' '.join(text)
    else:
        return "안녕하세요 elice_bot입니다.\n\"@elice_bot (찾고싶은 단어)\"를 입력해주세요."

    # 한국어 => 영어 결과를 위해 url 변환
    url_tmp = "endic.naver.com/search.nhn?sLn=kr&isOnlyViewEE=N&query=" + text
    url = "http://" + parse.quote(url_tmp)
    print(url)
    req = urllib.request.Request(url)
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    # 검색 결과 창의 태그 = "dt", 클래스 = "first"
    if soup.find("dt", class_="first"):
        if soup.find("span", class_="fnt_e30").find("strong"):
            # 검색 결과의 텍스트 가져오기
            result_text = soup.find("span", class_="fnt_e30").find("strong").get_text()
            # 구두점 제거
            for symbol in string.punctuation:
                result_text = result_text.replace(symbol, '')
            # 소문자로 입력 텍스트와 검색 결과 텍스트 비교
            if text.lower() == result_text.lower():
                message = "*" + result_text + " :*\t"
                # 검색 결과 테이블
                if soup.find("div", class_="align_right"):
                    result_table = soup.find("div", class_="align_right")
                    # 품사
                    if result_table.find("span", class_="fnt_k09"):
                        word_class = result_table.find("span", class_="fnt_k09")
                        message += word_class.get_text()
                    # 의미
                    if result_table.find("span", class_="fnt_k05"):
                        word_meaning = result_table.find("span", class_="fnt_k05")
                        message += word_meaning.get_text()
                    else:
                        "단어의 뜻을 모르겠습니다."
                    # 예문
                    if result_table.find("span", class_="fnt_e07 _ttsText"):
                        word_sentence = result_table.find("span", class_="fnt_e07 _ttsText")
                        message = message + '\n<예문> ' + word_sentence.get_text()
                    # 예문 번역
                    if result_table.find("span", class_="fnt_k10 _ttsText"):
                        word_sentence_meaning = result_table.find("span", class_="fnt_k10 _ttsText")
                        message = message + '  ' + word_sentence_meaning.get_text()
            else:
                message = "*" + text + "* 에 대한 검색결과가 없습니다. 정확한 단어를 입력해주세요."
        else:
            message = "*" + text + "* 에 대한 검색결과가 없습니다. 정확한 단어를 입력해주세요."
    else:
        message = "*" + text + "* 에 대한 검색결과가 없습니다. 정확한 단어를 입력해주세요."

    return message


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    post_message = _crawl_portal_keywords(text)
    slack_web_client.chat_postMessage(
        channel=channel,
        text=post_message,
    )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=5000)
